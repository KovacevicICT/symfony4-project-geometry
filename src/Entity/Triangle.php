<?php

namespace App\Entity;

/**
 * Class Triangle
 * @package App\Entity
 */
class Triangle implements GeometricShape
{
    const TYPE = "triangle";

    /**
     * @var float
     */
    private $a;

    /**
     * @var float
     */
    private $b;

    /**
     * @var float
     */
    private $c;

    /**
     * @var string
     */
    private $type;

    /**
     * @var float
     */
    private $surface;

    /**
     * @var float
     */
    private $circumference;

    /**
     * Triangle constructor.
     * @param float $a
     * @param float $b
     * @param float $c
     */
    public function __construct(
        float $a,
        float $b,
        float $c
    ) {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    /**
     * @return float
     */
    public function getA(): float
    {
        return $this->a;
    }

    /**
     * @return float
     */
    public function getB(): float
    {
        return $this->b;
    }

    /**
     * @return float
     */
    public function getC(): float
    {
        return $this->c;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getSurface(): float
    {
        return $this->surface;
    }

    /**
     * @param float $surface
     */
    public function setSurface(float $surface)
    {
        $this->surface = $surface;
    }

    /**
     * @return float
     */
    public function getCircumference(): float
    {
        return $this->circumference;
    }

    /**
     * @param float $circumference
     */
    public function setCircumference(float $circumference)
    {
        $this->circumference = $circumference;
    }

    /**
     * @return float
     */
    public function calculateSurface(): float
    {
        $s = ($this->getA()+$this->getB()+$this->getC())/2;
        return sqrt($s*(($s-$this->getA())*($s-$this->getB())*($s-$this->getC())));
    }

    /**
     * @return float
     */
    public function calculateCircumference(): float
    {
        return ($this->getA()+$this->getB()+$this->getC());
    }

    /**
     * @param $a
     * @param $b
     * @param $c
     * @return bool
     */
    public static function isTriangle($a, $b, $c): bool
    {
        return (($a + $b > $c) && ($a + $c > $b) && ($c + $b > $a));
    }
}
