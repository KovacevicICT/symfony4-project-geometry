<?php

namespace App\Entity;

/**
 * Class Circle
 * @package App\Entity
 */
class Circle implements GeometricShape
{
    const TYPE = "circle";

    /**
     * @var float
     */
    private $radius;

    /**
     * @var string
     */
    private $type;

    /**
     * @var float
     */
    private $surface;

    /**
     * @var float
     */
    private $circumference;

    /**
     * Circle constructor.
     * @param float $radius
     */
    public function __construct(
        float $radius
    ) {
        $this->radius = $radius;
    }

    /**
     * @return float
     */
    public function getRadius(): float
    {
        return $this->radius;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getSurface(): float
    {
        return $this->surface;
    }

    /**
     * @param float $surface
     */
    public function setSurface(float $surface)
    {
        $this->surface = $surface;
    }

    /**
     * @return float
     */
    public function getCircumference(): float
    {
        return $this->circumference;
    }

    /**
     * @param float $circumference
     */
    public function setCircumference(float $circumference)
    {
        $this->circumference = $circumference;
    }

    /**
     * @return float
     */
    public function calculateSurface(): float
    {
        return pow($this->radius, 2) * pi();
    }

    /**
     * @return float
     */
    public function calculateCircumference(): float
    {
        return $this->radius * 2 * pi();
    }
}
