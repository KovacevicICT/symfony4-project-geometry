<?php

namespace App\Entity;

/**
 * Interface GeometricShape
 * @package App\Entity
 */
interface GeometricShape
{
    /**
     * @return float
     */
    public function calculateSurface(): float;

    /**
     * @return float
     */
    public function calculateCircumference(): float;
}
