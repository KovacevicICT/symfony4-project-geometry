<?php

namespace App\ValueObject\Messages;

/**
 * Class MessageEnum
 * @package App\ValueObject\Messages
 */
class MessageEnum
{
    /**
     * missing number or float, zero given
     */
    const ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN = 'ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN';
    const ERR_NOT_TRIANGLE                  = 'ERR_NOT_TRIANGLE';
}
