<?php

namespace App\ValueObject\Command;

use Assert\Assert;
use App\Entity\Triangle;
use App\Exception\NotTriangleException;
use App\ValueObject\Messages\MessageEnum;

/**
 * Class CalculateTriangleCommand
 * @package App\ValueObject\Command
 */
class CalculateTriangleCommand
{
    /**
     * @var float
     */
    private $a;
    /**
     * @var float
     */
    private $b;
    /**
     * @var float
     */
    private $c;

    /**
     * CalculateTriangleCommand constructor.
     * @param float $a
     * @param float $b
     * @param float $c
     */
    public function __construct(
        float $a,
        float $b,
        float $c
    ) {
        $this->a = $a;
        $this->b = $b;
        $this->c = $c;
    }

    /**
     * @return float
     */
    public function getA(): float
    {
        return $this->a;
    }

    /**
     * @return float
     */
    public function getB(): float
    {
        return $this->b;
    }

    /**
     * @return float
     */
    public function getC(): float
    {
        return $this->c;
    }

    /**
     * @param float $a
     * @param float $b
     * @param float $c
     * @return CalculateTriangleCommand
     * @throws NotTriangleException
     */
    public static function sanitize(
        float $a,
        float $b,
        float $c
    )
    {
        Assert::lazy()
            ->tryAll()
            ->that($a, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->that($b, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->that($c, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->verifyNow();

        if (!Triangle::isTriangle($a, $b, $c)) {
            throw new NotTriangleException();
        }

        return new CalculateTriangleCommand($a, $b, $c);
    }
}
