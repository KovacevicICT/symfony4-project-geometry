<?php

namespace App\ValueObject\Command;

use Assert\Assert;
use App\ValueObject\Messages\MessageEnum;

/**
 * Class CalculateCircleCommand
 * @package App\ValueObject\Command
 */
class CalculateCircleCommand
{
    /**
     * @var float
     */
    private $radius;

    /**
     * CalculateCircleCommand constructor.
     * @param float $radius
     */
    public function __construct(
        float $radius
    ) {
        $this->radius = $radius;
    }

    /**
     * @return float
     */
    public function radius(): float
    {
        return $this->radius;
    }

    /**
     * @param float $float
     * @return CalculateCircleCommand
     */
    public static function sanitize(float $float): CalculateCircleCommand
    {
        Assert::lazy()
            ->tryAll()
            ->that($float, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->verifyNow();

        return new CalculateCircleCommand($float);
    }
}
