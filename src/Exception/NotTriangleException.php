<?php

namespace App\Exception;

use Exception;
use App\ValueObject\Messages\MessageEnum;

/**
 * Class NotTriangle
 * @package App\Exception
 */
class NotTriangleException extends Exception
{
    /**
     * NotTriangle constructor.
     */
    public function __construct()
    {
        parent::__construct(MessageEnum::ERR_NOT_TRIANGLE);
    }
}
