<?php

namespace App\Controller;

use App\Entity\Circle;
use App\Representation\GeometryRepresentation;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\ValueObject\Command\CalculateCircleCommand;

/**
 * Class CalculateCircle
 * @package App\Controller
 * @Route(
 *     "/circle/{radius}",
 *     name="circle",
 *     methods={"GET"}
 * )
 */
class CalculateCircle
{
    /**
     * @var GeometryRepresentation
     */
    private $representation;

    /**
     * CalculateCircle constructor.
     * @param GeometryRepresentation $representation
     */
    public function __construct(
        GeometryRepresentation $representation
    ) {
        $this->representation = $representation;
    }

    /**
     * @param float $radius
     * @return JsonResponse
     */
    public function __invoke(float $radius): JsonResponse
    {
        $command = CalculateCircleCommand::sanitize($radius);
        $circle = new Circle(
            $command->radius()
        );
        $circle->setType(Circle::TYPE);
        $circle->setSurface($circle->calculateSurface());
        $circle->setCircumference($circle->calculateCircumference());

        return $this->representation
            ->circleRepresentation(
                $circle
            );
    }
}
