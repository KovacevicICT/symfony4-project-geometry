<?php

namespace App\Controller;

use App\Entity\Triangle;
use App\Representation\GeometryRepresentation;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\ValueObject\Command\CalculateTriangleCommand;

/**
 * Class CalculateTriangle
 * @package App\Controller
 * @Route(
 *     "/triangle/{a}/{b}/{c} ",
 *     name="triangle",
 *     methods={"GET"}
 * )
 */
class CalculateTriangle
{
    /**
     * @var GeometryRepresentation
     */
    private $representation;

    /**
     * CalculateTriangle constructor.
     * @param GeometryRepresentation $representation
     */
    public function __construct(
        GeometryRepresentation $representation
    ) {
        $this->representation = $representation;
    }

    /**
     * @param float $a
     * @param float $b
     * @param float $c
     * @return JsonResponse
     */
    public function __invoke(float $a, float $b, float $c): JsonResponse
    {
        $command = CalculateTriangleCommand::sanitize($a, $b, $c);
        $triangle = new Triangle(
            $command->getA(),
            $command->getB(),
            $command->getC()
        );
        $triangle->setType(Triangle::TYPE);
        $triangle->setSurface($triangle->calculateSurface());
        $triangle->setCircumference($triangle->calculateCircumference());

        return $this->representation
            ->triangleRepresentation(
                $triangle
            );
    }
}
