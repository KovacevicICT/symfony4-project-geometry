<?php

namespace App\Service;

/**
 * Class MathematicalOperation
 * @package App\Service
 */
class MathematicalOperation implements MathematicalOperations
{
    /**
     * @param array $numbers
     * @return float
     */
    public function summarize(array $numbers): float
    {
        return array_sum($numbers);
    }
}
