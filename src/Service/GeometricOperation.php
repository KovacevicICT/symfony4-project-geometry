<?php

namespace App\Service;

use Assert\Assert;
use App\ValueObject\Messages\MessageEnum;

/**
 * Class GeometricOperation
 * @package App\Service
 */
class GeometricOperation
{
    /**
     * @var MathematicalOperation
     */
    private $mathematicalOperationInterface;

    /**
     * GeometricOperation constructor.
     * @param MathematicalOperation $mathematicalOperationInterface
     */
    public function __construct(
        MathematicalOperation $mathematicalOperationInterface
    ) {
        $this->mathematicalOperationInterface = $mathematicalOperationInterface;
    }

    /**
     * @param float $numOne
     * @param float $numTwo
     * @return float
     */
    public function sumTwoSurface(float $numOne, float $numTwo): float
    {
        Assert::lazy()
            ->tryAll()
            ->that($numOne, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->that($numTwo, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->verifyNow();

        return $this->mathematicalOperationInterface
            ->summarize([$numOne, $numTwo]);
    }

    /**
     * @param float $numOne
     * @param float $numTwo
     * @return float
     */
    public function sumTwoCircumference(float $numOne, float $numTwo): float
    {
        Assert::lazy()
            ->tryAll()
            ->that($numOne, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->that($numTwo, null)
            ->notEmpty(MessageEnum::ERR_NUMBER_IS_REQUIRED_ZERO_GIVEN)
            ->verifyNow();

        return $this->mathematicalOperationInterface
            ->summarize([$numOne, $numTwo]);
    }
}
