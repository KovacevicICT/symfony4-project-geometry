<?php

namespace App\Service;

/**
 * Interface MathematicalOperations
 * @package App\Service
 */
interface MathematicalOperations
{
    /**
     * @param array $numbers
     * @return float
     */
    public function summarize(array $numbers): float;
}
