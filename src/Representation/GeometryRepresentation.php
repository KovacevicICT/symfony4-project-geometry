<?php

namespace App\Representation;

use App\Entity\Circle;
use App\Entity\Triangle;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class GeometryRepresentation
 * @package App\Representation
 */
class GeometryRepresentation implements Representation
{
    /**
     * @param Triangle $triangle
     * @return JsonResponse
     */
    public function triangleRepresentation(Triangle $triangle): JsonResponse
    {
        return new JsonResponse(
            [
                "type" => $triangle->getType(),
                "a" => $this->formatFloat($triangle->getA(), 2),
                "b" => $this->formatFloat($triangle->getB(), 2),
                "c" => $this->formatFloat($triangle->getC(), 2),
                "surface" => $this->formatFloat($triangle->getSurface(), 2),
                "circumference" => $this->formatFloat($triangle->getCircumference(), 2)
            ]
        );
    }

    /**
     * @param Circle $circle
     * @return JsonResponse
     */
    public function circleRepresentation(Circle $circle)
    {
        return new JsonResponse(
            [
                "type"          => $circle->getType(),
                "radius"        => $this->formatFloat($circle->getRadius(), 2),
                "surface"       => $this->formatFloat($circle->getSurface(), 2),
                "circumference" => $this->formatFloat($circle->getCircumference(), 2)
            ]
        );
    }

    /**
     * ovdje sam stavio da vraća string, inače bi vraćao float sa svim decimalnim mjestima
     * izabrao sam ovo rješenje jer u FE imaju odlične parsere koji prebace u float vrijednosti
     * ajd da full stack nekad dobro dođe ;-)
     *
     * @param float $float
     * @param int $decimals
     * @return string
     */
    public function formatFloat(float $float, int $decimals): string
    {
        return number_format($float, $decimals, '.', '');
    }
}
