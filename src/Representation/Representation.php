<?php

namespace App\Representation;

/**
 * Interface Representation
 * @package App\Representation
 */
interface Representation
{
    /**
     * @param float $float
     * @param int $decimals
     * @return string
     */
    public function formatFloat(float $float, int $decimals): string;
}
